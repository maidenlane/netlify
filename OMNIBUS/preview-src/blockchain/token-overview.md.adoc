== A Unique Network Transport Infrastructure


=== Protocol access via API

*Raw EDI Transactions* Here is an API to interact with the "EDI Layer"/raw transaction protocol: https://app.swaggerhub.com/apis-docs/freight-chain/XEDI/2

*Mapping Interface/Parser* Here is an API to interact with the mapped transactions from the EDI Layer:  https://app.swaggerhub.com/apis-docs/freight-chain/primitiveAPI/2

=== Comparisons

Here is our biggest competitor in the "normal edi message VAN space"https://www.kleinschmidt.com/ks

Another competitor in the "normal edi message VAN space" https://www.livingstonintl.com/resource/paperless-billing-four-options-livingston-clients/

We offer the same service, at a significant cost savings, plus additional functionality (if they want to).

'''

=== EDI Messages Priced as Ethereum (per kilobyte/gwei)

|===
| Yellow Paper | Gwei | EDI Transaction | Cost in gwei

| GxdataZero
| 4
| Segments
| unit256

| GxdataNonZero
| 6,800
| Interchange
| 1,136

| GxminTransaction
| 21,000
| File Data
| 77,248
|===


[discrete]
===== Calculating Max Supply

We then dervice a rough estimate of EDI messages sent per block by the following:

*Genesis file* Gas limit:	0xa00000 gas limit per block (decimal): 10485760

We then Calculate the EDI messages sent per block by using the template validation EDI file for 211 (bill of lading)

EDI message (in bytes): 1,136 EDI Transaction cost: 77,248 $EDI tx's per block: 135.7415079

Max Transcactions per year per 1 token: 428,358,824.8

 	   151,113,902
 	 + 428,358,825 Summation: 611,029,679

Citations and Sources:  
https://github.com/nsward/evm-opcodes 
http://gavwood.com/paper.pdf
https://github.com/freight-chain/node/blob/master/genesis.json
https://gist.github.com/sambacha/3bccb5af0dc834254df0935ba9cedb9c